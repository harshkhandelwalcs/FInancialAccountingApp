import { Component } from '@angular/core';
import { IonicPage, App, NavController, NavParams } from 'ionic-angular';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { ReminderPage } from '../../pages/settings/reminder/reminder';

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-settings',
	templateUrl: 'settings.html',
})
export class SettingsPage {
	// @ViewChild(ReminderPage) reminderPage: ReminderPage

	time: any = localStorage.getItem('time');
	constructor(public appCtrl: App, private localNotifications: LocalNotifications, public navCtrl: NavController, public navParams: NavParams) {
console.log(this.time)
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad SettingsPage');
	}

	public schedule(ev) {
		if (ev._disabled == false) {
			this.localNotifications.schedule({
				id: 1,
				title: "Reminder",
				text: 'Hey! Its a reminder for your study',
				smallIcon: 'res://calendar',
				at: new Date(new Date().getTime() +this.time*60*60*1000),
				sound: 'file://sound.mp3'
			});
		}
	}

	reminderSet() {
		this.appCtrl.getRootNav().push(ReminderPage);
	}





}
