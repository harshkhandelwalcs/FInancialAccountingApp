import { Component } from '@angular/core';
import { IonicPage, App, ViewController, NavController, NavParams } from 'ionic-angular';
import { SettingsPage } from '../../pages/settings/settings';
import { BuyfullversionPage } from '../../pages/buyfullversion/buyfullversion';
import { EmailComposer } from '@ionic-native/email-composer';
@IonicPage()
@Component({
	selector: 'page-more',
	templateUrl: 'more.html',
})
export class MorePage {

	constructor(public navCtrl: NavController,
		public navParams: NavParams,
		public appCtrl: App,
		public viewCtrl: ViewController,
		private emailComposer: EmailComposer) {
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad MorePage');
	}
	buyFullVersion() {
		this.viewCtrl.dismiss();
		this.appCtrl.getRootNav().push(BuyfullversionPage);
	}
	needHelp() {
		this.viewCtrl.dismiss();
		let email = {
			to: 'hkcs1995@gmail.com',
			subject: 'feeedback',
		};
		this.emailComposer.open(email);
	}
	settings() {
		this.viewCtrl.dismiss();
		this.appCtrl.getRootNav().push(SettingsPage);
	}


}
