import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GovernmentAccountingAndReportingContPage } from './government-accounting-and-reporting-cont';

@NgModule({
  declarations: [
    GovernmentAccountingAndReportingContPage,
  ],
  imports: [
    IonicPageModule.forChild(GovernmentAccountingAndReportingContPage),
  ],
})
export class GovernmentAccountingAndReportingContPageModule {}
