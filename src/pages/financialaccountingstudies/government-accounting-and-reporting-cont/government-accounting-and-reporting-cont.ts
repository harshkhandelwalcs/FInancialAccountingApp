import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the GovernmentAccountingAndReportingContPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-government-accounting-and-reporting-cont',
  templateUrl: 'government-accounting-and-reporting-cont.html',
})
export class GovernmentAccountingAndReportingContPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GovernmentAccountingAndReportingContPage');
  }

}
