import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OtherFinancialStatementPresentationsPage } from './other-financial-statement-presentations';

@NgModule({
  declarations: [
    OtherFinancialStatementPresentationsPage,
  ],
  imports: [
    IonicPageModule.forChild(OtherFinancialStatementPresentationsPage),
  ],
})
export class OtherFinancialStatementPresentationsPageModule {}
