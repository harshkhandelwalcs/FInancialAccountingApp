import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GovernmentAccountingAndReportingPage } from './government-accounting-and-reporting';

@NgModule({
  declarations: [
    GovernmentAccountingAndReportingPage,
  ],
  imports: [
    IonicPageModule.forChild(GovernmentAccountingAndReportingPage),
  ],
})
export class GovernmentAccountingAndReportingPageModule {}
