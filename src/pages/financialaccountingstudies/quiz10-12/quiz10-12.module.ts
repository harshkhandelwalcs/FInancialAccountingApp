import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Quiz10_12Page } from './quiz10-12';

@NgModule({
  declarations: [
    Quiz10_12Page,
  ],
  imports: [
    IonicPageModule.forChild(Quiz10_12Page),
  ],
})
export class Quiz10_12PageModule {}
