import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RevisionTab3Page } from './revision-tab3';

@NgModule({
  declarations: [
    RevisionTab3Page,
  ],
  imports: [
    IonicPageModule.forChild(RevisionTab3Page),
  ],
})
export class RevisionTab3PageModule {}
