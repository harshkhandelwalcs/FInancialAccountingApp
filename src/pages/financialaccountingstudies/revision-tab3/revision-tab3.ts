import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the RevisionTab3Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-revision-tab3',
  templateUrl: 'revision-tab3.html',
})
export class RevisionTab3Page {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    debugger
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RevisionTab3Page');
  }

}
