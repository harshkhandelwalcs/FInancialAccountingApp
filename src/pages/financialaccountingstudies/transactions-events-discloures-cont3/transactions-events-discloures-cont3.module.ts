import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TransactionsEventsDisclouresCont3Page } from './transactions-events-discloures-cont3';

@NgModule({
  declarations: [
    TransactionsEventsDisclouresCont3Page,
  ],
  imports: [
    IonicPageModule.forChild(TransactionsEventsDisclouresCont3Page),
  ],
})
export class TransactionsEventsDisclouresCont3PageModule {}
