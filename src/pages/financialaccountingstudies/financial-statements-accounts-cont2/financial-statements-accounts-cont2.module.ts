import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FinancialStatementsAccountsCont2Page } from './financial-statements-accounts-cont2';

@NgModule({
  declarations: [
    FinancialStatementsAccountsCont2Page,
  ],
  imports: [
    IonicPageModule.forChild(FinancialStatementsAccountsCont2Page),
  ],
})
export class FinancialStatementsAccountsCont2PageModule {}
