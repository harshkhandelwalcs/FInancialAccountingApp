import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Quiz7_9Page } from './quiz7-9';

@NgModule({
  declarations: [
    Quiz7_9Page,
  ],
  imports: [
    IonicPageModule.forChild(Quiz7_9Page),
  ],
})
export class Quiz7_9PageModule {}
