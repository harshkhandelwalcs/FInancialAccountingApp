import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FinancialReportingAndPresentationPage } from './financial-reporting-and-presentation';

@NgModule({
  declarations: [
    FinancialReportingAndPresentationPage,
  ],
  imports: [
    IonicPageModule.forChild(FinancialReportingAndPresentationPage),
  ],
})
export class FinancialReportingAndPresentationPageModule {}
