import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SecReportingRequirementsPage } from './sec-reporting-requirements';

@NgModule({
  declarations: [
    SecReportingRequirementsPage,
  ],
  imports: [
    IonicPageModule.forChild(SecReportingRequirementsPage),
  ],
})
export class SecReportingRequirementsPageModule {}
