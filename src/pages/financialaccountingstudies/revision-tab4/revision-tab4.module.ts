import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RevisionTab4Page } from './revision-tab4';

@NgModule({
  declarations: [
    RevisionTab4Page,
  ],
  imports: [
    IonicPageModule.forChild(RevisionTab4Page),
  ],
})
export class RevisionTab4PageModule {}
