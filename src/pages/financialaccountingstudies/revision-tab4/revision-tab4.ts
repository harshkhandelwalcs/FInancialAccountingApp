import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the RevisionTab4Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-revision-tab4',
  templateUrl: 'revision-tab4.html',
})
export class RevisionTab4Page {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    debugger
  }
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad RevisionTab4Page');
  }

}
