import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FinancialStatementAccountsPage } from './financial-statement-accounts';

@NgModule({
  declarations: [
    FinancialStatementAccountsPage,
  ],
  imports: [
    IonicPageModule.forChild(FinancialStatementAccountsPage),
  ],
})
export class FinancialStatementAccountsPageModule {}
