import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the NonprofitAccountingAndReportingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-nonprofit-accounting-and-reporting',
  templateUrl: 'nonprofit-accounting-and-reporting.html',
})
export class NonprofitAccountingAndReportingPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NonprofitAccountingAndReportingPage');
  }

}
