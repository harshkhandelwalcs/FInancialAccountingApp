import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NonprofitAccountingAndReportingPage } from './nonprofit-accounting-and-reporting';

@NgModule({
  declarations: [
    NonprofitAccountingAndReportingPage,
  ],
  imports: [
    IonicPageModule.forChild(NonprofitAccountingAndReportingPage),
  ],
})
export class NonprofitAccountingAndReportingPageModule {}
