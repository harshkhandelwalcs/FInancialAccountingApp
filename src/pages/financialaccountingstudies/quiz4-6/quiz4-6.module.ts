import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Quiz4_6Page } from './quiz4-6';

@NgModule({
  declarations: [
    Quiz4_6Page,
  ],
  imports: [
    IonicPageModule.forChild(Quiz4_6Page),
  ],
})
export class Quiz4_6PageModule {}
