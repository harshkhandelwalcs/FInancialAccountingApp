import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FinancialStatementsAccountsCont1Page } from './financial-statements-accounts-cont1';

@NgModule({
  declarations: [
    FinancialStatementsAccountsCont1Page,
  ],
  imports: [
    IonicPageModule.forChild(FinancialStatementsAccountsCont1Page),
  ],
})
export class FinancialStatementsAccountsCont1PageModule {}
