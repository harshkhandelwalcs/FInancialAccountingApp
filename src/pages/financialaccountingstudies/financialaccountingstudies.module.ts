import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FinancialaccountingstudiesPage } from './financialaccountingstudies';

@NgModule({
  declarations: [
    FinancialaccountingstudiesPage,
  ],
  imports: [
    IonicPageModule.forChild(FinancialaccountingstudiesPage),
  ],
})
export class FinancialaccountingstudiesPageModule {}
