import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RevisionTab2Page } from './revision-tab2';

@NgModule({
  declarations: [
    RevisionTab2Page,
  ],
  imports: [
    IonicPageModule.forChild(RevisionTab2Page),
  ],
})
export class RevisionTab2PageModule {}
