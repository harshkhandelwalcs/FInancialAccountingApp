import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the RevisionTab2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-revision-tab2',
  templateUrl: 'revision-tab2.html',
})
export class RevisionTab2Page {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    debugger
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RevisionTab2Page');
  }

}
