import { Component } from '@angular/core';
import { IonicPage, App, NavController, NavParams } from 'ionic-angular';
import { StandardSettingPage } from '../financialaccountingstudies/standard-setting/standard-setting';
import { ConceptualFrameworkPage } from '../financialaccountingstudies/conceptual-framework/conceptual-framework';
import { FinancialReportingAndPresentationPage } from '../financialaccountingstudies/financial-reporting-and-presentation/financial-reporting-and-presentation';
import { Quiz1_3Page } from '../financialaccountingstudies/quiz1-3/quiz1-3';
import { FinancialStatementAccountsPage } from '../financialaccountingstudies/financial-statement-accounts/financial-statement-accounts';
import { OtherFinancialStatementPresentationsPage } from '../financialaccountingstudies/other-financial-statement-presentations/other-financial-statement-presentations';
import { SecReportingRequirementsPage } from '../financialaccountingstudies/sec-reporting-requirements/sec-reporting-requirements';
import { Quiz4_6Page } from '../financialaccountingstudies/quiz4-6/quiz4-6';
import { FinancialStatementsAccountsCont1Page } from '../financialaccountingstudies/financial-statements-accounts-cont1/financial-statements-accounts-cont1';
import { FinancialStatementsAccountsCont2Page } from '../financialaccountingstudies/financial-statements-accounts-cont2/financial-statements-accounts-cont2';
import { FinancialStatementsAccountsCont3Page } from '../financialaccountingstudies/financial-statements-accounts-cont3/financial-statements-accounts-cont3';
import { Quiz7_9Page } from '../financialaccountingstudies/quiz7-9/quiz7-9';
import { TransactionsEventsDisclouresPage } from '../financialaccountingstudies/transactions-events-discloures/transactions-events-discloures';
import { TransactionsEventsDisclouresCont1Page } from '../financialaccountingstudies/transactions-events-discloures-cont1/transactions-events-discloures-cont1';
import { TransactionsEventsDisclouresCont2Page } from '../financialaccountingstudies/transactions-events-discloures-cont2/transactions-events-discloures-cont2';
import { Quiz10_12Page } from '../financialaccountingstudies/quiz10-12/quiz10-12';
import { TransactionsEventsDisclouresCont3Page } from '../financialaccountingstudies/transactions-events-discloures-cont3/transactions-events-discloures-cont3';
import { GovernmentAccountingAndReportingPage } from '../financialaccountingstudies/government-accounting-and-reporting/government-accounting-and-reporting';
import { GovernmentAccountingAndReportingContPage } from '../financialaccountingstudies/government-accounting-and-reporting-cont/government-accounting-and-reporting-cont';
import { Quiz13_15Page } from '../financialaccountingstudies/quiz13-15/quiz13-15';
import { NonprofitAccountingAndReportingPage } from '../financialaccountingstudies/nonprofit-accounting-and-reporting/nonprofit-accounting-and-reporting';
import { FinalExamPage } from '../financialaccountingstudies/final-exam/final-exam';
import { SharedProvider } from '../../providers/shared/shared';

@IonicPage()
@Component({
	selector: 'page-financialaccountingstudies',
	templateUrl: 'financialaccountingstudies.html',
})
export class FinancialaccountingstudiesPage {
	constructor(public navCtrl: NavController,
		public appCtrl: App,
		public navParams: NavParams,
		public shared: SharedProvider) {
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad FinancialaccountingstudiesPage');
	}
	gotoPage(page) {
		this.shared.pageName = page;
		this.shared.sharedData(0);
		if (page == 'standard-setting') {
			this.appCtrl.getRootNav().push(StandardSettingPage, { id: "standard setting" });
		}else if (page == 'conceptual-framewok') {

			this.appCtrl.getRootNav().push(ConceptualFrameworkPage)
		}else if (page == 'conceptual-framework') {
			this.appCtrl.getRootNav().push(ConceptualFrameworkPage);
		}
		else if (page == 'financial-reporting-and-presentation') {
			this.appCtrl.getRootNav().push(FinancialReportingAndPresentationPage);
		}
		else if (page == 'quiz1_3') {
			this.appCtrl.getRootNav().push(Quiz1_3Page);
		}
		else if (page == 'sec-reporting-requirements') {
			this.appCtrl.getRootNav().push(SecReportingRequirementsPage);
		}
		else if (page == 'other-financial-statement-presentations') {
			this.appCtrl.getRootNav().push(OtherFinancialStatementPresentationsPage);
		}
		else if (page == 'financial-statement-accounts') {
			this.appCtrl.getRootNav().push(FinancialStatementAccountsPage);
		}
		else if (page == 'quiz4_6') {
			this.appCtrl.getRootNav().push(Quiz4_6Page);
		}
		else if (page == 'financial-statements-accounts-cont1') {
			this.appCtrl.getRootNav().push(FinancialStatementsAccountsCont1Page);
		}
		else if (page == 'financial-statements-accounts-cont2') {
			this.appCtrl.getRootNav().push(FinancialStatementsAccountsCont2Page);
		}
		else if (page == 'financial-statements-accounts-cont3') {
			this.appCtrl.getRootNav().push(FinancialStatementsAccountsCont3Page);
		}
		else if (page == 'quiz7_9') {
			this.appCtrl.getRootNav().push(Quiz7_9Page);
		}
		else if (page == 'transactions-events-discloures') {
			this.appCtrl.getRootNav().push(TransactionsEventsDisclouresPage);
		}
		else if (page == 'transactions-events-discloures-cont1') {
			this.appCtrl.getRootNav().push(TransactionsEventsDisclouresCont1Page);
		}
		else if (page == 'transactions-events-discloures-cont2') {
			this.appCtrl.getRootNav().push(TransactionsEventsDisclouresCont2Page);
		}
		else if (page == 'quiz10_12') {
			this.appCtrl.getRootNav().push(Quiz10_12Page);
		}
		else if (page == 'transactions-events-discloures-cont3') {
			this.appCtrl.getRootNav().push(TransactionsEventsDisclouresCont3Page);
		}
		else if (page == 'government-accounting-and-reporting') {
			this.appCtrl.getRootNav().push(GovernmentAccountingAndReportingPage);
		}
		else if (page == 'government-accounting-and-reporting-cont') {
			this.appCtrl.getRootNav().push(GovernmentAccountingAndReportingContPage);
		}
		else if (page == 'quiz13_15') {
			this.appCtrl.getRootNav().push(Quiz13_15Page);
		}
		else if (page == 'nonprofit-accounting-and-reporting') {
			this.appCtrl.getRootNav().push(NonprofitAccountingAndReportingPage);
		}
		else if (page == 'final-exam') {
			this.appCtrl.getRootNav().push(FinalExamPage);
		}
	}
}
