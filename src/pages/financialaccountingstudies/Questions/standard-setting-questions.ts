

export const Questions = {
    'standard-setting': {
        '0': [
            {
                q1: "Hi",
                answer: "hello"
            },
            {
                q1: "Hi",
                answer: "hello"
            },
            {
                q1: "Hi",
                answer: "hello"
            }
        ],
        '1': [{
            q1: "Hi",
            answer: "hello"
        }],
        '2': [{
            q1: "Hi",
            answer: "hello"
        }],
        '3': [{
            q1: "Hi",
            answer: "hello"
        }]
    },
    'conceptual-framework': {
        '0': [{
            q1: "Hi",
            answer: "hello"
        }],
        '1': [],
        '2': [],
        '3': []
    },
    'financial-reporting-and-presentation': {
        '0': [{
            q1: "Hi",
            answer: "hello"
        }],
        '1': [],
        '2': [],
        '3': []
    },
    'quiz1_3': {
        '0': [{
            q1: "Hi",
            answer: "hello"
        }],
        '1': [],
        '2': [],
        '3': []
    },
    'sec-reporting-requirements': {
        '0': [{
            q1: "Hi",
            answer: "hello"
        }],
        '1': [],
        '2': [],
        '3': []
    },
    'other-financial-statement-presentations': {
        '0': [{
            q1: "Hi",
            answer: "hello"
        }],
        '1': [],
        '2': [],
        '3': []
    },
    'financial-statement-accounts': {
        '0': [{
            q1: "Hi",
            answer: "hello"
        }],
        '1': [],
        '2': [],
        '3': []
    },
    'quiz4_6': {
        '0': [{
            q1: "Hi",
            answer: "hello"
        }],
        '1': [],
        '2': [],
        '3': []
    },
    'financial-statements-accounts-cont1': {
        '0': [{
            q1: "Hi",
            answer: "hello"
        }],
        '1': [],
        '2': [],
        '3': []
    },
    'financial-statements-accounts-cont2': {
        '0': [{
            q1: "Hi",
            answer: "hello"
        }],
        '1': [],
        '2': [],
        '3': []
    },
    'financial-statements-accounts-cont3': {
        '0': [{
            q1: "Hi",
            answer: "hello"
        }],
        '1': [],
        '2': [],
        '3': []
    },
    'quiz7_9': {
        '0': [{
            q1: "Hi",
            answer: "hello"
        }],
        '1': [],
        '2': [],
        '3': []
    },
    'transactions-events-discloures': {
        '0': [{
            q1: "Hi",
            answer: "hello"
        }],
        '1': [],
        '2': [],
        '3': []
    },
    'transactions-events-discloures-cont1': {
        '0': [{
            q1: "Hi",
            answer: "hello"
        }],
        '1': [],
        '2': [],
        '3': []
    },
    'transactions-events-discloures-cont2': {
        '0': [{
            q1: "Hi",
            answer: "hello"
        }],
        '1': [],
        '2': [],
        '3': []
    },
    'quiz10_12': {
        '0': [{
            q1: "Hi",
            answer: "hello"
        }],
        '1': [],
        '2': [],
        '3': []
    },
    'transactions-events-discloures-cont3': {
        '0': [{
            q1: "Hi",
            answer: "hello"
        }],
        '1': [],
        '2': [],
        '3': []
    },
    'government-accounting-and-reporting': {
        '0': [{
            q1: "Hi",
            answer: "hello"
        }],
        '1': [],
        '2': [],
        '3': []
    },
    'government-accounting-and-reporting-cont': {
        '0': [{
            q1: "Hi",
            answer: "hello"
        }],
        '1': [],
        '2': [],
        '3': []
    },
    'quiz13_15': {
        '0': [{
            q1: "Hi",
            answer: "hello"
        }],
        '1': [],
        '2': [],
        '3': []
    },
    'nonprofit-accounting-and-reporting': {
        '0': [{
            q1: "Hi",
            answer: "hello"
        }],
        '1': [],
        '2': [],
        '3': []
    },
    'final-exam': {
        '0': [{
            q1: "Hi",
            answer: "hello"
        }],
        '1': [],
        '2': [],
        '3': []
    }
}