import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FinancialStatementsAccountsCont3Page } from './financial-statements-accounts-cont3';

@NgModule({
  declarations: [
    FinancialStatementsAccountsCont3Page,
  ],
  imports: [
    IonicPageModule.forChild(FinancialStatementsAccountsCont3Page),
  ],
})
export class FinancialStatementsAccountsCont3PageModule {}
