import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FinancialStatementsAccountsCont3Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-financial-statements-accounts-cont3',
  templateUrl: 'financial-statements-accounts-cont3.html',
})
export class FinancialStatementsAccountsCont3Page {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FinancialStatementsAccountsCont3Page');
  }

}
