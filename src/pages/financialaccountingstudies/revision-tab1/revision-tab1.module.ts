import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RevisionTab1Page } from './revision-tab1';

@NgModule({
  declarations: [
    RevisionTab1Page,
  ],
  imports: [
    IonicPageModule.forChild(RevisionTab1Page),
  ],
})
export class RevisionTab1PageModule {}
