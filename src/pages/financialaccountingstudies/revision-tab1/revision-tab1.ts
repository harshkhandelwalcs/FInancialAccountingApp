import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SharedProvider } from '../../../providers/shared/shared';

/**
 * Generated class for the RevisionTab1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-revision-tab1',
  templateUrl: 'revision-tab1.html',
})
export class RevisionTab1Page {
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public shared: SharedProvider) {
      debugger

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RevisionTab1Page');

  }

}
