import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Quiz13_15Page } from './quiz13-15';

@NgModule({
  declarations: [
    Quiz13_15Page,
  ],
  imports: [
    IonicPageModule.forChild(Quiz13_15Page),
  ],
})
export class Quiz13_15PageModule {}
