import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TransactionsEventsDisclouresCont1Page } from './transactions-events-discloures-cont1';

@NgModule({
  declarations: [
    TransactionsEventsDisclouresCont1Page,
  ],
  imports: [
    IonicPageModule.forChild(TransactionsEventsDisclouresCont1Page),
  ],
})
export class TransactionsEventsDisclouresCont1PageModule {}
