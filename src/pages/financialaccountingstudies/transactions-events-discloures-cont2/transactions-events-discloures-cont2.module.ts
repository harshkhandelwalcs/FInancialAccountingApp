import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TransactionsEventsDisclouresCont2Page } from './transactions-events-discloures-cont2';

@NgModule({
  declarations: [
    TransactionsEventsDisclouresCont2Page,
  ],
  imports: [
    IonicPageModule.forChild(TransactionsEventsDisclouresCont2Page),
  ],
})
export class TransactionsEventsDisclouresCont2PageModule {}
