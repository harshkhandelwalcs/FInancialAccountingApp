import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RevisionTabsComponent } from '../../../components/revision-tabs/revision-tabs';



/**
 * Generated class for the StandardSettingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-standard-setting',
  templateUrl: 'standard-setting.html',
})
export class StandardSettingPage {
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    let data = navParams.get('id');
    console.log(data)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StandardSettingPage');
  }

}
