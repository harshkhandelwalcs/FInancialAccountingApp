import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StandardSettingPage } from './standard-setting';

@NgModule({
  declarations: [
    StandardSettingPage,
  ],
  imports: [
    IonicPageModule.forChild(StandardSettingPage),
  ],
})
export class StandardSettingPageModule {}
