import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TransactionsEventsDisclouresPage } from './transactions-events-discloures';

@NgModule({
  declarations: [
    TransactionsEventsDisclouresPage,
  ],
  imports: [
    IonicPageModule.forChild(TransactionsEventsDisclouresPage),
  ],
})
export class TransactionsEventsDisclouresPageModule {}
