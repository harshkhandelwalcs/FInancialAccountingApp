import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConceptualFrameworkPage } from './conceptual-framework';

@NgModule({
  declarations: [
    ConceptualFrameworkPage,
  ],
  imports: [
    IonicPageModule.forChild(ConceptualFrameworkPage),
  ],
})
export class ConceptualFrameworkPageModule {}
