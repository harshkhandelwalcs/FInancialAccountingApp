import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Quiz1_3Page } from './quiz1-3';

@NgModule({
  declarations: [
    Quiz1_3Page,
  ],
  imports: [
    IonicPageModule.forChild(Quiz1_3Page),
  ],
})
export class Quiz1_3PageModule {}
