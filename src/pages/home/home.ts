import { Component } from '@angular/core';
import { NavController, PopoverController } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { ExambuilderPage } from '../exambuilder/exambuilder';
import { FinancialaccountingstudiesPage } from '../financialaccountingstudies/financialaccountingstudies';
import { PracticetestsPage } from '../practicetests/practicetests';
import { MorePage } from '../more/more';
@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage {
	dynamicHeader = ["Financial Accounting Studies", "Practice Tests", "Exambuilder"]
	title = "Financial Accounting Studies";
	rootPage1: any;
	rootPage2: any;
	rootPage3: any;
	isAndroid: boolean = false;
	constructor(public navCtrl: NavController,
		platform: Platform,
		public popoverCtrl: PopoverController) {
		this.isAndroid = platform.is('android');
		this.rootPage1 = FinancialaccountingstudiesPage;
		this.rootPage2 = PracticetestsPage;
		this.rootPage3 = ExambuilderPage;
	}


	onTabSelect(ev: any) {
		this.title = this.dynamicHeader[ev.index];
	}

	presentPopover(ev) {
		let popover = this.popoverCtrl.create(MorePage, {
			//contentEle: this.content.nativeElement,
			// textEle: this.text.nativeElement
		});
		popover.present({
			ev: ev
		});
	}
}
