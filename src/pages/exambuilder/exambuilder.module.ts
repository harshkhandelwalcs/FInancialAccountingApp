import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExambuilderPage } from './exambuilder';

@NgModule({
  declarations: [
    ExambuilderPage,
  ],
  imports: [
    IonicPageModule.forChild(ExambuilderPage),
  ],
})
export class ExambuilderPageModule {}
