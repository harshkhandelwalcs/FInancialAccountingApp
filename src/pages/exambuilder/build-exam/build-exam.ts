import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
/**
 * Generated class for the BuildExamPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-build-exam',
  templateUrl: 'build-exam.html',
})
export class BuildExamPage {
  public filter = this.navParams.get('filter');
  public noOfQuestions = this.navParams.get('noOfQuestionsSelected');
  public noOfMinutes = this.navParams.get('noOfMinutesSelected');
  public selectedOption = this.navParams.get('selectedOption');
  constructor(public navCtrl: NavController,
    public navParams: NavParams    ) {
    
    console.log(this.filter)
    console.log(this.noOfQuestions)
    console.log(this.noOfMinutes)
    console.log(this.selectedOption)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BuildExamPage');
  }

}
