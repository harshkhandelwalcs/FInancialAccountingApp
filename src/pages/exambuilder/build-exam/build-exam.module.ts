import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BuildExamPage } from './build-exam';

@NgModule({
  declarations: [
    BuildExamPage,
  ],
  imports: [
    IonicPageModule.forChild(BuildExamPage),
  ],
})
export class BuildExamPageModule {}
