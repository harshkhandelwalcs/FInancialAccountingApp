import { Component } from '@angular/core';
import { IonicPage, NavController, App, NavParams } from 'ionic-angular';
import { BuildExamPage } from './build-exam/build-exam';

@IonicPage()
@Component({
	selector: 'page-exambuilder',
	templateUrl: 'exambuilder.html',
})
export class ExambuilderPage {
	names: any;
	selectedAll:boolean=false;
	selectOptionFlag:boolean=false;
	selectOption:any;
	noOfQuestionsSelected: any = 5;
	noOfMinutesSelected: any = 5;
	filter: any = "missedQuestions";
	constructor(public navCtrl: NavController,
		public navParams: NavParams,
		public appCtrl: App) {
		// this.names = [
		// 	{ name: 'Practice Test 1', selected: false },
		// 	{ name: 'Practice Test 2', selected: false },
		// 	{ name: 'Practice Test 3', selected: false },
		// 	{ name: 'Practice Test 4', selected: false },
		// ]
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad ExambuilderPage');
	}
	// selectAll() {
	// 	for (var i = 0; i < this.names.length; i++) {
	// 		this.names[i].selected = this.selectedAll;
	// 	}
	// }
	// checkIfAllSelected(value, name) {
	// 	console.log(value.srcElement.checked)
	// 	if (name) {
	// 		this.selectOptionFlag=value.srcElement.checked;
	// 		this.selectOption = name;
	// 	}

	// 	this.selectedAll = this.names.every((item: any) => {
	// 		return item.selected == true;
	// 	})
	// }
	buildExam() {
	    console.log("hi")
		console.log("selectedAll", this.selectedAll)
		console.log("selectedOption",this.selectOption)
		console.log("noOfQuestionsSelected", this.noOfQuestionsSelected)
		console.log("noOfMinutesSelected", this.noOfMinutesSelected)
		console.log("filter", this.filter)
		this.appCtrl.getRootNav().push(BuildExamPage,{
			selectedOption:this.selectOption,
			noOfQuestionsSelected:this.noOfQuestionsSelected,
			noOfMinutesSelected:this.noOfMinutesSelected,
			filter:this.filter
		});
	}

}
