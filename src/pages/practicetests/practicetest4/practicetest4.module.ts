import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Practicetest4Page } from './practicetest4';

@NgModule({
  declarations: [
    Practicetest4Page,
  ],
  imports: [
    IonicPageModule.forChild(Practicetest4Page),
  ],
})
export class Practicetest4PageModule {}
