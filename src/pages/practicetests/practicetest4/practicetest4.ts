import { Component } from '@angular/core';
import { IonicPage, App, NavController, NavParams } from 'ionic-angular';
import { PracticetestSettingsPage } from '../../../pages/practicetests/practicetest-settings/practicetest-settings';

/**
 * Generated class for the Practicetest4Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-practicetest4',
  templateUrl: 'practicetest4.html',
})
export class Practicetest4Page {
  questions: any
  constructor(public navCtrl: NavController, public navParams: NavParams, public appCtrl: App) {
    if (this.navParams.get('questions')) {
      debugger
      this.questions = this.navParams.get('questions');
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Practicetest4Page');
  }
  settings() {
    this.appCtrl.getRootNav().push(PracticetestSettingsPage);
  }
}
