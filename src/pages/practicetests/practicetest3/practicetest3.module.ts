import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Practicetest3Page } from './practicetest3';

@NgModule({
  declarations: [
    Practicetest3Page,
  ],
  imports: [
    IonicPageModule.forChild(Practicetest3Page),
  ],
})
export class Practicetest3PageModule {}
