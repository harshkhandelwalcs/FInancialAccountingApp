import { Component } from '@angular/core';
import { IonicPage, App, NavController, NavParams } from 'ionic-angular';
import { Practicetest1Page } from '../../pages/practicetests/practicetest1/practicetest1';
import { Practicetest2Page } from '../../pages/practicetests/practicetest2/practicetest2';
import { Practicetest3Page } from '../../pages/practicetests/practicetest3/practicetest3';
import { Practicetest4Page } from '../../pages/practicetests/practicetest4/practicetest4';
import { SharedProvider } from '../../providers/shared/shared';
@IonicPage()
@Component({
	selector: 'page-practicetests',
	templateUrl: 'practicetests.html',
})
export class PracticetestsPage {
	currentPracticetest: any;
	constructor(public navCtrl: NavController,
		public navParams: NavParams,
		public appCtrl: App,
		public shared: SharedProvider) {
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad PracticetestsPage');
	}
	practiceTest1() {
		let data = this.shared.getQuestions("0", "practiceTest1");
		debugger
		// localStorage.setItem("Questions",data)
		this.appCtrl.getRootNav().push(Practicetest1Page, { "questions": data, "testname": "practiceTest1" });
		this.currentPracticetest = 1
	}
	practiceTest2() {
		let data = this.shared.getQuestions("1", "practiceTest2");
		debugger
		// localStorage.setItem("Questions",data)
		this.appCtrl.getRootNav().push(Practicetest2Page, { "questions": data, "testname": "practiceTest2" });
		this.currentPracticetest = 2;
	}

	practiceTest3() {
		let data = this.shared.getQuestions("3", "practiceTest3");
		debugger
		// localStorage.setItem("Questions",data)
		this.appCtrl.getRootNav().push(Practicetest3Page, { "questions": data, "testname": "practiceTest3" });
		this.currentPracticetest = 3;
	}
	practiceTest4() {
		let data = this.shared.getQuestions("4", "practiceTest4");
		debugger
		// localStorage.setItem("Questions",data)
		this.appCtrl.getRootNav().push(Practicetest4Page, { "questions": data, "testname": "practiceTest4" });
		this.currentPracticetest = 4;
	}
}
