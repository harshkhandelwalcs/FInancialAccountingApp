import { Component, Input } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PracticePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-practice',
	templateUrl: 'practice.html',
})
export class PracticePage {
	questions: any;
	questionArray: any;
	isCorrect: any = false;
	constructor(public navCtrl: NavController, public navParams: NavParams) {
		debugger
		if (this.navParams) {
			this.questions = this.navParams;
			if (this.questions &&
				this.questions.data &&
				this.questions.data.data &&
				this.questions.data.data.practiceTest1) {
				debugger
				this.questionArray = this.questions.data.data.practiceTest1;
			} else if (this.questions &&
				this.questions.data &&
				this.questions.data.data &&
				this.questions.data.data.practiceTest2) {
				debugger
				this.questionArray = this.questions.data.data.practiceTest2;
			} else if (this.questions &&
				this.questions.data &&
				this.questions.data.data &&
				this.questions.data.data.practiceTest3) {
				debugger
				this.questionArray = this.questions.data.data.practiceTest3;
			} else if (this.questions &&
				this.questions.data &&
				this.questions.data.data &&
				this.questions.data.data.practiceTest4) {
				debugger
				this.questionArray = this.questions.data.data.practiceTest4;

			}
		}
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad PracticePage');
	}
	mcqAnswer(selectedValue, answer) {
		debugger
		console.log(selectedValue, answer)
		if (selectedValue == answer) {
			debugger
			this.isCorrect = true;
		} else {
			this.isCorrect = false;
		}

	}

}
