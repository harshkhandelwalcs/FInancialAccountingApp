import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PracticetestsPage } from './practicetests';

@NgModule({
  declarations: [
    PracticetestsPage,
  ],
  imports: [
    IonicPageModule.forChild(PracticetestsPage),
  ],
})
export class PracticetestsPageModule {}
