import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PracticetestSettingsPage } from './practicetest-settings';

@NgModule({
  declarations: [
    PracticetestSettingsPage,
  ],
  imports: [
    IonicPageModule.forChild(PracticetestSettingsPage),
  ],
})
export class PracticetestSettingsPageModule {}
