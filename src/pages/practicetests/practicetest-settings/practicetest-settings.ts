import { Component } from '@angular/core';
import { IonicPage, App, NavController, NavParams } from 'ionic-angular';
import { BuyfullversionPage } from '../../../pages/buyfullversion/buyfullversion';
import { ToastController } from 'ionic-angular';
/**
 * Generated class for the PracticetestSettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-practicetest-settings',
	templateUrl: 'practicetest-settings.html',
})
export class PracticetestSettingsPage{
	saturation:any = 10;
	range: any = 10;
	constructor(public navCtrl: NavController,public toastCtrl: ToastController, public navParams: NavParams, public appCtrl: App) {
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad PracticetestSettingsPage');
	}

	getRange(ev) {

		if (ev._valA < 6) {
			this.saturation = 6;
			this.range = 6;
		} else {
			this.range = ev._valA;
		}

	}
	getOpenQuestions() {
		if (this.range) {
			this.appCtrl.getRootNav().push(BuyfullversionPage);
			let toast = this.toastCtrl.create({
				message: 'Buy full version to change number of questions each test',
				duration: 3000
			  });
			  toast.present();
		}
	}
}
