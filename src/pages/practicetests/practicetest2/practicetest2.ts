import { Component } from '@angular/core';
import { IonicPage, App, NavController, NavParams } from 'ionic-angular';
import { PracticetestSettingsPage } from '../../../pages/practicetests/practicetest-settings/practicetest-settings';

/**
 * Generated class for the Practicetest2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-practicetest2',
  templateUrl: 'practicetest2.html',
})
export class Practicetest2Page {
  questions: any;
  testName: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public appCtrl: App) {
    if (this.navParams.get('questions') && this.navParams.get('testname')) {
      debugger
      this.testName = this.navParams.get('testname');
      this.questions = this.navParams.get('questions');
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Practicetest2Page');
  }
  settings() {
    this.appCtrl.getRootNav().push(PracticetestSettingsPage);
  }

}
