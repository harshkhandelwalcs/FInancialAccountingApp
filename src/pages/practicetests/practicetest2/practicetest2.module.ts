import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Practicetest2Page } from './practicetest2';

@NgModule({
  declarations: [
    Practicetest2Page,
  ],
  imports: [
    IonicPageModule.forChild(Practicetest2Page),
  ],
})
export class Practicetest2PageModule {}
