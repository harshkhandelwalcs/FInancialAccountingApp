import { Component } from '@angular/core';
import { IonicPage, App, NavController, NavParams } from 'ionic-angular';
import { PracticetestSettingsPage } from '../../../pages/practicetests/practicetest-settings/practicetest-settings';
import { PracticeTestTabsComponent } from '../../../components/practice-test-tabs/practice-test-tabs';

/**
 * Generated class for the Practicetest1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-practicetest1',
  templateUrl: 'practicetest1.html',
})
export class Practicetest1Page {
  questions: any;
  testName: any;
  constructor(public navCtrl: NavController, public appCtrl: App, public navParams: NavParams) {
    debugger
    if (this.navParams.get('questions') && this.navParams.get('testname')) {
      debugger
      this.testName = this.navParams.get('testname');
      this.questions = this.navParams.get('questions');
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Practicetest1Page');
  }
  settings() {
    this.appCtrl.getRootNav().push(PracticetestSettingsPage);
  }

}
