import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Practicetest1Page } from './practicetest1';

@NgModule({
  declarations: [
    Practicetest1Page,
  ],
  imports: [
    IonicPageModule.forChild(Practicetest1Page),
  ],
})
export class Practicetest1PageModule {}
