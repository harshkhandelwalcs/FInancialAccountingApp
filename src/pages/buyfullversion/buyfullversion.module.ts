import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BuyfullversionPage } from './buyfullversion';

@NgModule({
  declarations: [
    BuyfullversionPage,
  ],
  imports: [
    IonicPageModule.forChild(BuyfullversionPage),
  ],
})
export class BuyfullversionPageModule {}
