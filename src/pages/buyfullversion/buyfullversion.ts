import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { InAppPurchase } from '@ionic-native/in-app-purchase';
/**
 * Generated class for the BuyfullversionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-buyfullversion',
  templateUrl: 'buyfullversion.html',
})
export class BuyfullversionPage {
	error:any
	error1:any;
	products:any;
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
		private iap: InAppPurchase,
		public alertCtrl: AlertController) {
  }
  ionViewDidLoad() {
		console.log('ionViewDidLoad BuyfullversionPage');
  }
  paymentRecieve(){
	  debugger
		this.iap
		.getProducts(['paid_access']).then((products)=> {
			debugger
			console.log(products);
			this.products = products;
			console.log(this.products);
			return this.iap.buy('paid_access').then((data)=> {
				debugger
				console.log(data);
			}) 
			.catch(function (err) {
				debugger
				console.log("err",err);
			});
		
		})
	}

showAlert(data,msg) {
	let alert = this.alertCtrl.create({
		title:msg,
		subTitle: data,
		buttons: ['OK']
	});
	alert.present();
}
}
