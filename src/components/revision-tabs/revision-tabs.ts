import { Component } from '@angular/core';
import { RevisionTab1Page } from '../../pages/financialaccountingstudies/revision-tab1/revision-tab1';
import { RevisionTab2Page } from '../../pages/financialaccountingstudies/revision-tab2/revision-tab2';
import { RevisionTab3Page } from '../../pages/financialaccountingstudies/revision-tab3/revision-tab3';
import { RevisionTab4Page } from '../../pages/financialaccountingstudies/revision-tab4/revision-tab4';
import { SharedProvider } from '../../providers/shared/shared';

/**
 * Generated class for the RevisionTabsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
	selector: 'revision-tabs',
	templateUrl: 'revision-tabs.html'
})
export class RevisionTabsComponent {
	rootPage1: any;
	rootPage2: any;
	rootPage3: any;
	rootPage4: any;
	defaultSelectedIndex:any = 0;

	constructor(public shared:SharedProvider) {
		this.rootPage1 = RevisionTab1Page;
		this.rootPage2 = RevisionTab2Page;
		this.rootPage3 = RevisionTab3Page;
		this.rootPage4 = RevisionTab4Page

	}
	onTabSelect(ev: any) {
		debugger
		let index = this.defaultSelectedIndex;
		if(ev.index){
			index = ev.index;
		}
		this.shared.sharedData(index);

	}
}
