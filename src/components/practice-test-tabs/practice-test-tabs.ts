import { Component, Input } from '@angular/core';
import { PracticePage } from '../../pages/practicetests/practice/practice';
import { ReviewPage } from '../../pages/practicetests/review/review';
import { SharedProvider } from '../../providers/shared/shared';

/**
 * Generated class for the PracticeTestTabsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'practice-test-tabs',
  templateUrl: 'practice-test-tabs.html'
})
export class PracticeTestTabsComponent {

  rootPage1: any;
  rootPage2: any;
  defaultSelectedIndex: any = 0;
  questionData: any;
  testname: any;
  combinedData: any;
  @Input() set Questions(data: any) {
    this.questionData = data;
    debugger
  }
  @Input() set practiceTestName(data: any) {
    debugger
    this.testname = data;
    this.combinedData = {
      data: this.questionData,
      name: this.testname,
    }
  }
  constructor(public shared: SharedProvider) {
    debugger
    console.log('Hello PracticeTestTabsComponent Component');
    this.rootPage1 = PracticePage;
    this.rootPage2 = ReviewPage;
  }
  onTabSelect(ev: any) {
    let index = this.defaultSelectedIndex;
    if (ev.index) {
      index = ev.index;
    }
    console.log(index)
  }
}
