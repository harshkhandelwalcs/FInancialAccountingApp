import { NgModule } from '@angular/core';
import { RevisionTabsComponent } from './revision-tabs/revision-tabs';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { PracticeTestTabsComponent } from './practice-test-tabs/practice-test-tabs';

@NgModule({
	declarations: [RevisionTabsComponent,
    PracticeTestTabsComponent,
    ],
	imports: [SuperTabsModule.forRoot(),
	],
	exports: [RevisionTabsComponent,
    PracticeTestTabsComponent,
    ]
})
export class ComponentsModule {}
