import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Questions } from '../../pages/financialaccountingstudies/Questions/standard-setting-questions';
import { Http } from '@angular/http';
/*
  Generated class for the SharedProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SharedProvider {
	pageName: any;
	index: any;
	questionJson = Questions;
	sectionData: any;
	constructor(public http: Http) {
		debugger
		console.log('Hello SharedProvider Provider');
	}

	sharedData(index) {
		debugger
		this.sectionData = this.questionJson[this.pageName][index];
	}

	getQuestions(index,testname) {
		debugger
		let data = [
			{
				practiceTest1: [
					{
						question: "what is your name?",
						options: [
							{
								value: "harsh",
								label: "Harsh"
							},
							{
								value: "bhavesh",
								label: "Bhavesh"
							},
							{
								value: "govinda",
								label: "Govinda"
							},
							{
								value: "shiv",
								label: "Shiv"
							}
						],
						answer: "harsh"
					},
					{
						question: "what is your name?",
						options: [
							{
								value: "harsh",
								label: "Harsh"
							},
							{
								value: "bhavesh",
								label: "Bhavesh"
							},
							{
								value: "govinda",
								label: "Govinda"
							},
							{
								value: "shiv",
								label: "Shiv"
							}
						],
						answer: "harsh"
					}

				]
			},
			{
				practiceTest2: [
					{
						question: "what is your000 name?",
						options: [
							{
								value: "harsh",
								label: "Harsh"
							},
							{
								value: "bhavesh",
								label: "Bhavesh"
							},
							{
								value: "govinda",
								label: "Govinda"
							},
							{
								value: "shiv",
								label: "Shiv"
							}
						],
						answer: "harsh"
					},
					{
						question: "what is your name?",
						options: [
							{
								value: "harsh",
								label: "Harsh"
							},
							{
								value: "bhavesh",
								label: "Bhavesh"
							},
							{
								value: "govinda",
								label: "Govinda"
							},
							{
								value: "shiv",
								label: "Shiv"
							}
						],
						answer: "harsh"
					}

				]
			}
		]
		return data[index];
	}


}
