import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ExambuilderPage } from '../pages/exambuilder/exambuilder';
import { BuildExamPage } from '../pages/exambuilder/build-exam/build-exam';
import { HttpModule } from '@angular/http';

import { FinancialaccountingstudiesPage } from '../pages/financialaccountingstudies/financialaccountingstudies';
import { PracticetestsPage } from '../pages/practicetests/practicetests';
import { MorePage } from '../pages/more/more';
import { SettingsPage } from '../pages/settings/settings';
import { ComingSoonPage } from '../pages/coming-soon/coming-soon';
import { PracticetestSettingsPage } from '../pages/practicetests/practicetest-settings/practicetest-settings';
import { Quiz1_3Page } from '../pages/financialaccountingstudies/quiz1-3/quiz1-3';
import { Quiz4_6Page } from '../pages/financialaccountingstudies/quiz4-6/quiz4-6';
import { Quiz7_9Page } from '../pages/financialaccountingstudies/quiz7-9/quiz7-9';
import { Quiz10_12Page } from '../pages/financialaccountingstudies/quiz10-12/quiz10-12';
import { Quiz13_15Page } from '../pages/financialaccountingstudies/quiz13-15/quiz13-15';
import { ConceptualFrameworkPage } from '../pages/financialaccountingstudies/conceptual-framework/conceptual-framework';
import { FinancialReportingAndPresentationPage } from '../pages/financialaccountingstudies/financial-reporting-and-presentation/financial-reporting-and-presentation';
import { StandardSettingPage } from '../pages/financialaccountingstudies/standard-setting/standard-setting';
import { FinancialStatementAccountsPage } from '../pages/financialaccountingstudies/financial-statement-accounts/financial-statement-accounts';
import { OtherFinancialStatementPresentationsPage } from '../pages/financialaccountingstudies/other-financial-statement-presentations/other-financial-statement-presentations';
import { SecReportingRequirementsPage } from '../pages/financialaccountingstudies/sec-reporting-requirements/sec-reporting-requirements';
import { FinancialStatementsAccountsCont1Page } from '../pages/financialaccountingstudies/financial-statements-accounts-cont1/financial-statements-accounts-cont1';
import { FinancialStatementsAccountsCont2Page } from '../pages/financialaccountingstudies/financial-statements-accounts-cont2/financial-statements-accounts-cont2';
import { FinancialStatementsAccountsCont3Page } from '../pages/financialaccountingstudies/financial-statements-accounts-cont3/financial-statements-accounts-cont3';

import { TransactionsEventsDisclouresPage } from '../pages/financialaccountingstudies/transactions-events-discloures/transactions-events-discloures';
import { TransactionsEventsDisclouresCont1Page } from '../pages/financialaccountingstudies/transactions-events-discloures-cont1/transactions-events-discloures-cont1';
import { TransactionsEventsDisclouresCont2Page } from '../pages/financialaccountingstudies/transactions-events-discloures-cont2/transactions-events-discloures-cont2';
import { TransactionsEventsDisclouresCont3Page } from '../pages/financialaccountingstudies/transactions-events-discloures-cont3/transactions-events-discloures-cont3';
import { GovernmentAccountingAndReportingPage } from '../pages/financialaccountingstudies/government-accounting-and-reporting/government-accounting-and-reporting';
import { GovernmentAccountingAndReportingContPage } from '../pages/financialaccountingstudies/government-accounting-and-reporting-cont/government-accounting-and-reporting-cont';
import { NonprofitAccountingAndReportingPage } from '../pages/financialaccountingstudies/nonprofit-accounting-and-reporting/nonprofit-accounting-and-reporting';
import { FinalExamPage } from '../pages/financialaccountingstudies/final-exam/final-exam';
import { RevisionTab1Page } from '../pages/financialaccountingstudies/revision-tab1/revision-tab1';
import { RevisionTab2Page } from '../pages/financialaccountingstudies/revision-tab2/revision-tab2';
import { RevisionTab3Page } from '../pages/financialaccountingstudies/revision-tab3/revision-tab3';
import { RevisionTab4Page } from '../pages/financialaccountingstudies/revision-tab4/revision-tab4';
import { PracticePage } from '../pages/practicetests/practice/practice';
import { ReviewPage } from '../pages/practicetests/review/review';
import { ReminderPage } from '../pages/settings/reminder/reminder';
import { BuyfullversionPage } from '../pages/buyfullversion/buyfullversion';
import { Practicetest1Page } from '../pages/practicetests/practicetest1/practicetest1';
import { Practicetest2Page } from '../pages/practicetests/practicetest2/practicetest2';
import { Practicetest3Page } from '../pages/practicetests/practicetest3/practicetest3';
import { Practicetest4Page } from '../pages/practicetests/practicetest4/practicetest4';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { ComponentsModule } from '../components/components.module';
import { EmailComposer } from '@ionic-native/email-composer';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SharedProvider } from '../providers/shared/shared';
import { InAppPurchase } from '@ionic-native/in-app-purchase';
@NgModule({
	declarations: [
		MyApp,
		HomePage,
		ExambuilderPage,
		FinancialaccountingstudiesPage,
		PracticetestsPage,
		MorePage,
		SettingsPage,
		BuyfullversionPage,
		Practicetest1Page,
		Practicetest2Page,
		Practicetest3Page,
		Practicetest4Page,
		ReminderPage,
		ConceptualFrameworkPage,
		FinancialReportingAndPresentationPage,
		StandardSettingPage,
		ComingSoonPage,
		Quiz1_3Page,
		Quiz4_6Page,
		Quiz7_9Page,
		Quiz10_12Page,
		Quiz13_15Page,
		FinancialStatementAccountsPage,
		OtherFinancialStatementPresentationsPage,
		SecReportingRequirementsPage,
		FinancialStatementsAccountsCont1Page,
		FinancialStatementsAccountsCont2Page,
		FinancialStatementsAccountsCont3Page,
		TransactionsEventsDisclouresPage,
		TransactionsEventsDisclouresCont1Page,
		TransactionsEventsDisclouresCont2Page,
		TransactionsEventsDisclouresCont3Page,
		GovernmentAccountingAndReportingPage,
		GovernmentAccountingAndReportingContPage,
		NonprofitAccountingAndReportingPage,
		FinalExamPage,
		RevisionTab1Page,
		RevisionTab2Page,
		RevisionTab3Page,
		RevisionTab4Page,
		PracticetestSettingsPage,
		PracticePage,
		ReviewPage,
		BuildExamPage

	],
	imports: [
		BrowserModule,
		HttpModule,
		ComponentsModule,
		IonicModule.forRoot(MyApp),
		SuperTabsModule.forRoot(),

	],
	bootstrap: [IonicApp],
	entryComponents: [
		MyApp,
		HomePage,
		ExambuilderPage,
		FinancialaccountingstudiesPage,
		PracticetestsPage,
		MorePage,
		SettingsPage,
		BuyfullversionPage,
		Practicetest1Page,
		Practicetest2Page,
		Practicetest3Page,
		Practicetest4Page,
		ReminderPage,
		ConceptualFrameworkPage,
		FinancialReportingAndPresentationPage,
		StandardSettingPage,
		ComingSoonPage,
		Quiz1_3Page,
		Quiz4_6Page,
		Quiz7_9Page,
		Quiz10_12Page,
		Quiz13_15Page,
		FinancialStatementAccountsPage,
		OtherFinancialStatementPresentationsPage,
		SecReportingRequirementsPage,
		FinancialStatementsAccountsCont1Page,
		FinancialStatementsAccountsCont2Page,
		FinancialStatementsAccountsCont3Page,
		TransactionsEventsDisclouresPage,
		TransactionsEventsDisclouresCont1Page,
		TransactionsEventsDisclouresCont2Page,
		TransactionsEventsDisclouresCont3Page,
		GovernmentAccountingAndReportingPage,
		GovernmentAccountingAndReportingContPage,
		NonprofitAccountingAndReportingPage,
		FinalExamPage,
		RevisionTab1Page,
		RevisionTab2Page,
		RevisionTab3Page,
		RevisionTab4Page,
		PracticetestSettingsPage,
		PracticePage,
		ReviewPage,
		BuildExamPage
	],
	providers: [
		EmailComposer,
		LocalNotifications,
		StatusBar,
		InAppPurchase,
		SplashScreen,
		{ provide: ErrorHandler, useClass: IonicErrorHandler },
		SharedProvider
	]
})
export class AppModule { }
